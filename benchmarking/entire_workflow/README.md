Benchmarking an entire OVarFlow workflow
===========================================

To benchmark the entire workflow the respective computer was exclusively dedicated to the execution of OVarFlow. Only the usual background processes of a Linux system and an ssh session running top were causing additional load. Therefore an realistic impression of the system load caused by OVarFlow could be obtained. CPU and memory usage was measured every 30 seconds:

* CPU monitoring: ``mpstat 30 > mpstat_statistics &``
* memory monitoring: ``sar -r ALL 30 > sar_statistics &``

Evaluation of the obtained metrics was done in two steps:
1. A shell script, mainly relying on ``awk`` was used to convert the time format into continuous second intervals.
   ```
   ./format_time.sh <input data> > <formated data>
   ```
2. An R script was used for the further processing and plotting of the newly formatted data:
   ```
   ./plot_total.R <formated data sar> <formated data mpstat> <image name> <image headline>
   ```

Calculation of actual memory usage depends on the given distribution or kernel, respectively. Memory usage under Ubuntu 16.04 calculated as:
```
MemTotal - MemFree - Buffers - Cached - Slab
```
Further details can be found at the [Red Hat Customer Portal](https://access.redhat.com/solutions/406773).

In 20.04 memory usage as reported by sar (12.2) is directly reported as ``kbmemused``.
