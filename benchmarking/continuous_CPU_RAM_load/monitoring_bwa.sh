#!/usr/bin/env bash

FILE=$1
MON_DIR=monitor_bwa_${FILE}
INTERVAL=${3:-3} # default is to take a measurement every 3 seconds

# test if directory exists and add a number
# avoids overwriting of files in existing directory
counter=1
baseDir=$MON_DIR
while [[ -d $MON_DIR ]]
do
    MON_DIR=${baseDir}_$((counter++))
done
mkdir ${MON_DIR}

#********************************************
# command to be monitored: bwa mem & samtools
#********************************************
bwa mem -M -t 6 \
  -R "@RG\tID:id_${FILE}\tPL:illumina\tPU:dummy\tCN:SRR\tLB:lib_${FILE}\tSM:${FILE}" \
  processed_reference/GCF_000002315.6_GRCg6a_genomic.fa.gz FASTQ_INPUT_DIR/${FILE}_R1.fastq.gz \
  FASTQ_INPUT_DIR/${FILE}_R2.fastq.gz \
  2> ${MON_DIR}/${FILE}_stderr_bwa.log | \
samtools view -b /dev/fd/0 -o ${MON_DIR}/${FILE}.bam \
  2> ${MON_DIR}/${FILE}_stdout_samtools.log &
#****** Command END *************************

pid_sam=$!
pid_bwa=$((pid_sam - 1))
echo bwa $pid_bwa
echo sam $pid_sam

# loop while the sam process still exists
while ps -p $pid_sam > /dev/null
do
    # Unix time in seconds
    rightnow=$(date +%s)
    echo -n "bwa: "
    echo -en "$rightnow\t"
    ps -p $pid_bwa -o rss,%mem,%cpu | tail -1
    echo -n "samtools: "
    echo -en "$rightnow\t"
    ps -p $pid_sam -o rss,%mem,%cpu | tail -1
    # seconds to wait before the next measurement
    sleep $INTERVAL
done > ${MON_DIR}/resource_monitoring_${FILE}.log 2> /dev/null

#*** output format in log file **********
# tool, time (seconds), rss, % mem, % CPU
