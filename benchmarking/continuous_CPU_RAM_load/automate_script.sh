#!/usr/bin/env bash

echo "GatherVcfs"

./monitoring_script.sh GatherVcfs 2 2 SRR3041137
./monitoring_script.sh GatherVcfs 2 2 hSRR3041137

echo "SortSam"

./monitoring_script.sh SortSam 2 10 SRR3041137
./monitoring_script.sh SortSam 2 10 hSRR3041137

echo "MarkDuplicates"

./monitoring_script.sh MarkDuplicates 2 2 SRR3041137
./monitoring_script.sh MarkDuplicates 2 2 hSRR3041137

echo "HaplotypeCaller"

./monitoring_script.sh HaplotypeCaller 2 2 SRR3041137
./monitoring_script.sh HaplotypeCaller 2 2 hSRR3041137

echo Considerer clearing the PageCache before performing
echo another measurement. To do so, execute as root:
echo 'sync; echo 1 > /proc/sys/vm/drop_caches'

echo "********************"
echo "*     Ende         *"
echo "********************"
