#!/usr/bin/env bash

TOOL=$1
GC=$2
Xmx=$3
FILE=$4
MON_DIR=monitor_${TOOL}_${FILE}_GC${GC}_Xmx${Xmx}
INTERVAL=${5:-3} # default is take a measurement each 3 seconds

# test if directory exists and add a number
# avoids overwriting of files in existing directory
counter=1
baseDir=$MON_DIR
while [[ -d $MON_DIR ]]
do
    MON_DIR=${baseDir}_$((counter++))
done
mkdir ${MON_DIR}

#************************************
# select GATK command to be monitored
#************************************
case $TOOL in
SortSam)
    gatk --java-options "-Xmx${Xmx}G -XX:ParallelGCThreads=${GC}" SortSam \
        -I 01_mapping/${FILE}.bam \
	-SO coordinate \
	-O ${MON_DIR}/${FILE}.bam \
	--TMP_DIR ./GATK_tmp_dir/ 2> ${MON_DIR}/${FILE}.log &
    ;;
MarkDuplicates)
    gatk --java-options "-Xmx${Xmx}G -XX:ParallelGCThreads=${GC}" MarkDuplicates \
        -I 02_sort_gatk/${FILE}.bam \
	-O ${MON_DIR}/03_mark_duplicates_${FILE}.bam \
	-M ${MON_DIR}/03_mark_duplicates_${FILE}.txt \
	-MAX_FILE_HANDLES 300 --TMP_DIR ./GATK_tmp_dir/ 2> ${MON_DIR}/${FILE}.log &
    ;;
# a fixed interval / contig is monitored: NC_006093.5 => 36.374.701 bp
HaplotypeCaller)
    gatk --java-options "-Xmx${Xmx}G -XX:ParallelGCThreads=${GC}" HaplotypeCaller \
        -ERC GVCF \
	-I 03_mark_duplicates/${FILE}.bam \
	-R processed_reference/GCF_000002315.6_GRCg6a_genomic.fa.gz \
	-O ${MON_DIR}/${FILE}.gvcf.gz \
	-L "NC_006093.5" 2> ${MON_DIR}/${FILE}.log &
    ;;
CombineGVCFs)
    gatk --java-options "-Xmx${Xmx}G -XX:ParallelGCThreads=${GC}" CombineGVCFs \
        -O ${MON_DIR}/${FILE}.gvcf.gz \
        -R processed_reference/GCF_000002315.6_GRCg6a_genomic.fa.gz \
        -V 04_haplotypeCaller/${FILE}/interval_2.gvcf.gz \
        -V 04_haplotypeCaller/${FILE}/interval_4.gvcf.gz \
        -V 04_haplotypeCaller/${FILE}/interval_1.gvcf.gz \
        -V 04_haplotypeCaller/${FILE}/interval_3.gvcf.gz 2> ${MON_DIR}/${FILE}.log &
    ;;
GatherVcfs)
    gatk --java-options "-Xmx${Xmx}G -XX:ParallelGCThreads=${GC}" GatherVcfs \
        -I 04_haplotypeCaller/${FILE}/interval_1.g.vcf.gz \
        -I 04_haplotypeCaller/${FILE}/interval_2.g.vcf.gz \
        -I 04_haplotypeCaller/${FILE}/interval_3.g.vcf.gz \
        -I 04_haplotypeCaller/${FILE}/interval_4.g.vcf.gz \
        -I 04_haplotypeCaller/${FILE}/interval_5.g.vcf.gz \
        -O ${MON_DIR}/05_gathered_${FILE}.g.vcf.gz 2> ${MON_DIR}/${FILE}.log &
    ;;
BaseRecalibrator)
    gatk --java-options "-Xmx${Xmx}G -XX:ParallelGCThreads=${GC}" BaseRecalibrator \
        -R processed_reference/GCF_000002315.6_GRCg6a_genomic.fa.gz \
        -I 03_mark_duplicates/${FILE}.bam \
        --known-sites 11_filtered_removed_VCF/variants_filtered.vcf.gz \
	-O ${MON_DIR}/13_BQSR_${FILE}_recal_data.table 2> ${MON_DIR}/${FILE}.log &
    ;;
ApplyBQSR)
    gatk --java-options "-Xmx${Xmx}G -XX:ParallelGCThreads=${GC}" ApplyBQSR \
        -R processed_reference/GCF_000002315.6_GRCg6a_genomic.fa.gz \
        -I 03_mark_duplicates/${FILE}.bam \
        -bqsr 13_start_BQSR/${FILE}_recal_data.table \
        -O ${MON_DIR}/14_apply_BQSR_${FILE}_recal.bam 2> ${MON_DIR}/${FILE}.log &
    ;;
AnalyzeCovariates)
    gatk --java-options "-Xmx${Xmx}G -XX:ParallelGCThreads=${GC}" AnalyzeCovariates \
        -before 13_start_BQSR/${FILE}_recal_data.table \
        -after 15_analyze_BQSR/${FILE}_postBQSR_recal_data.table \
        -plots ${MON_DIR}/Analysis_${FILE}.pdf 2> ${MON_DIR}/${FILE}.log &
    ;;
*)
    echo "Unknown tool name specified. Exiting script."
    exit 2
esac
#****** GATK Command END ***********

gatk_pid=$!

# get pid of java process
# the process needs some time to be started
sleep 0.4
ps --ppid $gatk_pid
java_pid=`ps --ppid $gatk_pid | tail -1 | awk '{print $1}'`

echo PID of GATK: $gatk_pid
echo PID of java: $java_pid

# loop while the gatk process still exists
while ps -p $gatk_pid > /dev/null
do
    # Unix time in seconds
    rightnow=$(date +%s)
    echo -en "$rightnow\t"
    ps -p $java_pid -o rss,%mem,%cpu | tail -1
    # seconds to wait before the next measurement
    sleep $INTERVAL
done > ${MON_DIR}/resource_monitoring_${FILE}_GC${GC}_Xmx${Xmx}.log 2> /dev/null

#*** output format in log file ***
# time (seconds), rss, % mem, % CPU
