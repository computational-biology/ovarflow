#!/bin/bash 

#$ -q all.q 
#$ -cwd 
#$ -V
#$ -o output.txt 
#$ -e error.txt 
#$ -pe multislot 22
#$ -l virtual_free=6G 
#$ -l hostname=hp-sl230*

# die conada environment aktivieren
# dafür ist -V nötig sonst wird activate nicht gefunden
. activate /vol/agluehken/CondaEnvs/BM_10_Dec20

#./benchmarking_GATK_GC.sh SortSam ERR1092842 &
#./benchmarking_GATK_GC.sh MarkDuplicates ERR1092842 &
./benchmarking_GATK_GC.sh HaplotypeCaller ERR1092842 &
#./benchmarking_GATK_GC.sh HaplotypeCaller_interval ERR1092842 &
#./benchmarking_GATK_GC.sh CombineGVCFs ERR1092842 &
#./benchmarking_GATK_GC.sh GatherVcfs ERR1092842 &

wait

echo Ende
