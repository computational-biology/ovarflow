#!/usr/bin/env bash

# potential execution
# ./benchmarking_GATK_GC.sh SortSam SRR3041115 [Xmx_value]
TOOL=$1
FILE=$2
XMX=${3:-12} # default: 12 Gb Java heap size for gatk

TIME=`date +%Y.%m.%d_%T`
HOST=${HOSTNAME%%.*}

# reference genome to use - here from Drosphila
REF_GENOME=GCF_000001215.4_Release_6_plus_ISO1_MT_genomic.fa.gz
HC_CONTIG=NT_033779.5

# benchmarking shall not be limited by disk access
# all data shall be read and written to /dev/shm/GATK_BM
SHM_DIR=/dev/shm/GATK_BM_${TOOL}
# if the monitoring directory doesn't already exist, create it
if ! [[ -d $SHM_DIR ]]
then
    mkdir $SHM_DIR
# other wise remove it and start from the beginning
else
    rm -rf $SHM_DIR
    mkdir $SHM_DIR
fi
# function to be used in a trap for clean-up if script is aborted
cleanupSHM () {
    if [ -d ${SHM_DIR} ]
    then
        rm -rf ${SHM_DIR}
    fi

    exit
}
# trap is not invoked by qdel - reason unknown
trap cleanupSHM INT TERM

# benchmarking file
BM_FILE=BM_GC_${TOOL}_${TIME}_${HOST}

#************************************
# select GATK command to be monitored
#************************************
case $TOOL in
SortSam)
        #-O ${SHM_DIR}/${FILE}_${GC}.bam \
    GATK_CMD="SortSam \
	-I ${SHM_DIR}/01_mapping/${FILE}.bam \
	-SO coordinate \
        -O /dev/null \
        --TMP_DIR ${SHM_DIR}/GATK_tmp_dir"
    PageCachefiles="01_mapping/${FILE}.bam"
    ;;
MarkDuplicates)
        #-O ${SHM_DIR}/${FILE}_${GC}.bam \
    GATK_CMD="MarkDuplicates \
	-I ${SHM_DIR}/02_sort_gatk/${FILE}.bam \
        -O /dev/null \
        -M ${SHM_DIR}/${FILE}_\${GC}.txt \
        -MAX_FILE_HANDLES 300 \
        --TMP_DIR ${SHM_DIR}/GATK_tmp_dir"
    PageCachefiles="02_sort_gatk/${FILE}.bam"
    ;;
HaplotypeCaller)
    GATK_CMD="HaplotypeCaller -ERC GVCF \
        -I ${SHM_DIR}/03_mark_duplicates/${FILE}.bam \
        -R ${SHM_DIR}/processed_reference/${REF_GENOME} \
        -O ${SHM_DIR}/${FILE}_\${GC}.g.vcf.gz"
    PageCachefiles="03_mark_duplicates/${FILE}.bam 03_mark_duplicates/${FILE}.bam.bai processed_reference/${REF_GENOME} processed_reference/${REF_GENOME}.fai processed_reference/${REF_GENOME%fa.gz}dict processed_reference/${REF_GENOME}.gzi"
    ;;
HaplotypeCaller_interval)
    GATK_CMD="HaplotypeCaller -ERC GVCF \
        -I ${SHM_DIR}/03_mark_duplicates/${FILE}.bam \
        -R ${SHM_DIR}/processed_reference/${REF_GENOME} \
        -O ${SHM_DIR}/${FILE}_\${GC}.g.vcf.gz \
        -L ${HC_CONTIG}"
    PageCachefiles="03_mark_duplicates/${FILE}.bam 03_mark_duplicates/${FILE}.bam.bai processed_reference/${REF_GENOME} processed_reference/${REF_GENOME}.fai processed_reference/${REF_GENOME%fa.gz}dict processed_reference/${REF_GENOME}.gzi"
    ;;
CombineGVCFs)
    GATK_CMD="CombineGVCFs \
	-O ${SHM_DIR}/${FILE}_\${GC}.g.vcf.gz \
        -R ${SHM_DIR}/processed_reference/${REF_GENOME} \
        -V ${SHM_DIR}/04_haplotypeCaller/${FILE}/interval_2.g.vcf.gz \
        -V ${SHM_DIR}/04_haplotypeCaller/${FILE}/interval_4.g.vcf.gz \
        -V ${SHM_DIR}/04_haplotypeCaller/${FILE}/interval_1.g.vcf.gz \
        -V ${SHM_DIR}/04_haplotypeCaller/${FILE}/interval_3.g.vcf.gz \
        -V ${SHM_DIR}/04_haplotypeCaller/${FILE}/interval_5.g.vcf.gz \
        -V ${SHM_DIR}/04_haplotypeCaller/${FILE}/interval_6.g.vcf.gz"
    PageCachefiles="04_haplotypeCaller/${FILE}/interval_*.g.vcf.gz 04_haplotypeCaller/${FILE}/interval_*.g.vcf.gz.tbi processed_reference/${REF_GENOME} processed_reference/${REF_GENOME}.fai processed_reference/${REF_GENOME%fa.gz}dict processed_reference/${REF_GENOME}.gzi"
    ;;
GatherVcfs)
    GATK_CMD="GatherVcfs \
	-O ${SHM_DIR}/${FILE}_\${GC}.g.vcf.gz \
        -I ${SHM_DIR}/04_haplotypeCaller/${FILE}/interval_1.g.vcf.gz \
        -I ${SHM_DIR}/04_haplotypeCaller/${FILE}/interval_2.g.vcf.gz \
        -I ${SHM_DIR}/04_haplotypeCaller/${FILE}/interval_3.g.vcf.gz \
        -I ${SHM_DIR}/04_haplotypeCaller/${FILE}/interval_4.g.vcf.gz \
        -I ${SHM_DIR}/04_haplotypeCaller/${FILE}/interval_5.g.vcf.gz \
        -I ${SHM_DIR}/04_haplotypeCaller/${FILE}/interval_6.g.vcf.gz \
        --TMP_DIR ${SHM_DIR}/GATK_tmp_dir"
    PageCachefiles="04_haplotypeCaller/${FILE}/interval_*.g.vcf.gz processed_reference/${REF_GENOME}"
    ;;
*)
    echo "Unknown tool name specified: ${TOOL}. Exiting script."
    exit 2
esac

#***************************************
# execute and measure the actual command
#***************************************

# copy all accessed files already in memory (/dev/shm) before the measurement
echo -e "\nCopy files to /dev/shm"
cp --parents ${PageCachefiles} ${SHM_DIR}
echo -e "Copying finished.\n"

# also enable page caching for the application itself, by running it once
echo -e "\nPage cache run started."
gatk --java-options "-XX:ParallelGCThreads=2 -Xmx${XMX}G" ${GATK_CMD} &
GATKpid=$!
# let the GATK process run for 3 minutes, then abbort
# we don't want to wait hours for a complet HaplotyeCaller run
sleep 90 # let the GATK process run for 1 & 1/2 minute
kill -15 $GATKpid
kill -9 $GATKpid
wait # just to be sure, that background processes are finished
echo -e "Page cache run finished.\n"

# a certain number of gatk processes shall run in parallel
parallelJobs=2
count=0

for GC in 1 20 2 16 4 12 6 8
do
    eval echo "${GATK_CMD}"
    echo # just a blank line; \n doesn't work with eval

    # might be replaced by /usr/bin/time
    # for the Docker Image: /usr/local/envs/OVarFlow/bin/time
    eval /vol/agluehken/CondaEnvs/BM_10_Dec20/bin/time -o ${BM_FILE} --append -v \
        gatk --java-options "-XX:ParallelGCThreads=${GC}\ -Xmx${XMX}G" ${GATK_CMD} &

    # limit parallelization to number of $parallelJobs
    count=$((count+1))
    sleep 90 # in any case wait 90 sec between starting new gatk processes
    if [ $((count%parallelJobs)) -eq 0 ]
    then
	    wait
    fi
done

echo -en "\nend:"
date

# cleanup
rm -rf ${SHM_DIR}
