#!/usr/bin/env bash
#
# Konvertiert das Format der Ausgabe von GNU time
# in eine Liste, die die leichter lesbar ist und
# direkt in R eingelesen werden kann.
#
# usage:
# convert_format.sh <GNU time Eingabedatei>

input=$1

# Kopfzeile ausgeben
echo -e "user\tsys\twall\tRSS\tR.tMem\tXmx"

# Erklärung zum Befehl:
#   grep:  findet Zeilen mit: user time, system time, wall clock time & RSS
#   sed:   belässt nur die Zahl des Java Heap Space (Xmx) in der Zeile (Gb oder Mb)
#   sed:   belässt nur die gesuchten Zahlenwerte (user, sys, wall & RSS) in den Zeilen
#   sed:   entfernt: "	Runtime.totalMemory()="
#   paste: fügt alle zusammengehörigen Zahlen in einer Zeile zusammen
#          Ergebnis Xmx	user 	sys	wall		RSS
#          Bsp.     4 	2058.07	190.82	1:30:11.56	6262652
#   awk:   verwandelt die wall time (mm:sec.sec) in reine Sekundenangabe
#          if:    falls Format      min:sec.sec
#          else:  falls Format hour:min:sec.sec
#          Xmx ans Ende der Zeile stellen

cat $input| \
	grep -E '\-Xmx|time |Maximum resident|Runtime' | \
	sed 's/.*-Xmx\([0-9]*\)[G|M].*/\1/' | \
	sed 's/.*): *//' | \
	sed 's/.*()=//' | \
	paste - - - - - - | \
	awk 'BEGIN{OFS="\t"}
	     {split($4, array, ":")
	      if (length(array) == 3)
	          print $2, $3, array[1]*60*60 + array[2]*60 + array[3], $5, $6, $1
	      if (length(array) == 2)
	          print $2, $3, array[1]*60 + array[2], $5, $6, $1
	     }'
