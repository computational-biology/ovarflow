#!/usr/bin/env bash

# potential execution
# ./benchmarking_GATK_Xmx.sh SortSam SRR3041115 [4]
TOOL=$1
FILE=$2
GC=${3:-2} # default: 2 Java GC threads for gatk

# write results to benchmarking directory
BM_DIR=bm_Java_Xmx
# if the monitoring directory doesn't already exist, create it
if ! [[ -d $BM_DIR ]]
then
    mkdir $BM_DIR
fi

# benchmarking file
BM_FILE=${TOOL}_${FILE}_GC${GC}
# add a number if file already exists, avoids overwriting of existing files
counter=1
create_file=$BM_DIR/${BM_FILE}.bm
while [[ -f ${create_file} ]]
do
    create_file=${BM_DIR}/${BM_FILE}_$((counter++)).bm
done

TIME_OUT=$create_file

TMP_RES=${BM_DIR}/tmp_result
# function to be used in a trap for clean-up if script is abborted
cleanupTMP () {
    if [ -e ${TMP_RES}* ]
    then
        rm ${TMP_RES}*
    fi

    exit
}
trap cleanupTMP INT TERM

#************************************
# select GATK command to be monitored
#************************************
case $TOOL in
SortSam)
    GATK_CMD="SortSam -I 01_mapping/${FILE}.bam \
        -SO coordinate \
        -O ${TMP_RES}.bam \
        --TMP_DIR ./GATK_tmp_dir"
    PageCachefiles="01_mapping/${FILE}.bam"
    ;;
MarkDuplicates)
    GATK_CMD="MarkDuplicates -I 02_sort_gatk/${FILE}.bam \
        -O ${TMP_RES}.bam \
        -M ${TMP_RES}.txt \
        -MAX_FILE_HANDLES 300 \
        --TMP_DIR ./GATK_tmp_dir"
    PageCachefiles="02_sort_gatk/${FILE}.bam"
    ;;
HaplotypeCaller)
    GATK_CMD="HaplotypeCaller -ERC GVCF \
        -I 03_mark_duplicates/${FILE}.bam \
        -R processed_reference/GCF_000002315.6_GRCg6a_genomic.fa.gz \
        -O ${TMP_RES}.g.vcf.gz \
        -L NC_006093.5"
    PageCachefiles="03_mark_duplicates/${FILE}.bam processed_reference/GCF_000002315.6_GRCg6a_genomic.fa.gz"
    ;;
CombineGVCFs)
    GATK_CMD="CombineGVCFs -O ${TMP_RES}.g.vcf.gz \
        -R processed_reference/GCF_000002315.6_GRCg6a_genomic.fa.gz \
        -V 04_haplotypeCaller/${FILE}/interval_2.g.vcf.gz \
        -V 04_haplotypeCaller/${FILE}/interval_4.g.vcf.gz \
        -V 04_haplotypeCaller/${FILE}/interval_1.g.vcf.gz \
        -V 04_haplotypeCaller/${FILE}/interval_3.g.vcf.gz"
    PageCachefiles="04_haplotypeCaller/${FILE}/interval_1.g.vcf.gz 04_haplotypeCaller/${FILE}/interval_2.g.vcf.gz 04_haplotypeCaller/${FILE}/interval_3.g.vcf.gz 04_haplotypeCaller/${FILE}/interval_4.g.vcf.gz processed_reference/GCF_000002315.6_GRCg6a_genomic.fa.gz"
    ;;
GatherVcfs)
    GATK_CMD="GatherVcfs -O ${TMP_RES}.g.vcf.gz \
        -I 04_haplotypeCaller/${FILE}/interval_1.g.vcf.gz \
        -I 04_haplotypeCaller/${FILE}/interval_2.g.vcf.gz \
        -I 04_haplotypeCaller/${FILE}/interval_3.g.vcf.gz \
        -I 04_haplotypeCaller/${FILE}/interval_4.g.vcf.gz \
        --TMP_DIR ./GATK_tmp_dir"
    PageCachefiles="04_haplotypeCaller/${FILE}/interval_1.g.vcf.gz 04_haplotypeCaller/${FILE}/interval_2.g.vcf.gz 04_haplotypeCaller/${FILE}/interval_3.g.vcf.gz 04_haplotypeCaller/${FILE}/interval_4.g.vcf.gz"
    ;;
*)
    echo "Unknown tool name specified: ${TOOL}. Exiting script."
    exit 2
esac

#***************************************
# execute and measure the actual command
#***************************************

# load all accessed files already in memory (page cache) before the measurement
echo "PageCache hot start"
echo $PageCachefiles
cat $PageCachefiles > /dev/null
# also enable page caching for the application itself, by running it once
echo -e "\tRun the command for some seconds."
gatk --java-options "-XX:ParallelGCThreads=${GC} -Xmx12G" ${GATK_CMD} 2> /dev/null &
GATKpid=$!
sleep 40 # GatherVcfs should finish quickly
if ps -p $GATKpid > /dev/null ; then sleep 90; fi # otherwise wait a little longer
kill -15 $GATKpid 2> /dev/null
kill -9 $GATKpid 2> /dev/null
wait # just to be sure, that background processes are finished
echo -e "hot start done\n"

counter=1
for XMX in `echo '1 2 4 6 8 12 16 24 32 48'{,,}`
do
    echo -n "Iteration: $((counter++)) - "; date
    echo "  Java heap size: $XMX"
    echo "  Java Garbage Collection threads to use: $GC  <= fixed"

    # might be replaced by /usr/bin/time
    # for the Docker Image: /usr/local/envs/OVarFlow/bin/time
    /home/ubuntu/Volume/MIXED_COVERAGE/OVclone/bin/time -o ${TIME_OUT} --append -v \
        gatk --java-options "-XX:ParallelGCThreads=${GC} -Xmx${XMX}G" ${GATK_CMD} 2> ${TIME_OUT%bm}log

    grep "Runtime.totalMemory()" ${TIME_OUT%bm}log >> ${TIME_OUT}

    # remove the result produced by the GATK command
    rm ${TMP_RES}*
done
