#!/usr/bin/env bash

trap "exit" INT

echo -e "\nBenchmaring of SortSam"
echo -e "======================\n"
./benchmarking_GATK_Xmx.sh SortSam SRR3041137 2

echo -e "\nBenchmaring of MarkDuplicates"
echo -e "=============================\n"
./benchmarking_GATK_Xmx.sh MarkDuplicates SRR3041137 2

echo -e "\nBenchmaring of HaplotypeCaller"
echo -e "==============================\n"
./benchmarking_GATK_Xmx.sh HaplotypeCaller SRR3041137 2

echo -e "\nBenchmaring of CombineGVCFs"
echo -e "===========================\n"
./benchmarking_GATK_Xmx.sh CombineGVCFs SRR3041137 2

echo -e "\nBenchmaring of GatherVcfs"
echo -e "=========================\n"
./benchmarking_GATK_Xmx.sh GatherVcfs SRR3041137 2
