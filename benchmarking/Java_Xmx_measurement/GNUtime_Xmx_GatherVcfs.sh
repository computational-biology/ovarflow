#/usr/bin/env bash

# Skript um die verbrauchte Rechenzeit je nach Einstellung
# des Java Garbage Heap Space (-Xmx) zu messen.

# Verzeichnis zur temporären Speicherung der Ergebnisse:
BASE_DIR=TMP_DIR_GATHERVCFS
# Datei zum Speicher der Ausgabe von GNU time
TIME_OUT=GNUtime_Xmx_result_GatherVcfs
# Datei die analysiert werden soll
FILE=SRR3041137

# Java GC fix für die jeweilige Messung
GC=2

for iteration in {1..3}
do
    echo Iteration: $iteration

    for XMX in 2 4 6 8 12 16 24 32 48
    do
        DIR=${BASE_DIR}/${iteration}_Xmx${XMX}
        mkdir -p $DIR
        echo Java Garbage Collection threads to use: $GC
        echo Java memory settings to use: -Xmx${XMX}G
    
        /home/ubuntu/Volume/MIXED_COVERAGE/OVclone/bin/time -o $TIME_OUT --append -v \
           gatk --java-options "-Xmx${XMX}G -XX:ParallelGCThreads=${GC}" GatherVcfs \
           -O ${DIR}/05_gathered_samples_${FILE}.gvcf.gz \
           -I 04_haplotypeCaller/${FILE}/interval_1.g.vcf.gz \
           -I 04_haplotypeCaller/${FILE}/interval_2.g.vcf.gz \
           -I 04_haplotypeCaller/${FILE}/interval_3.g.vcf.gz \
           -I 04_haplotypeCaller/${FILE}/interval_4.g.vcf.gz \
           --TMP_DIR ./GATK_tmp_dir 2> ${DIR}/05_gathered_samples_${FILE}.log

    
        grep "Runtime.totalMemory()" ${DIR}/*.log >> $TIME_OUT
        # Die Ergebnisse der Berechnung werden nicht benötigt
        #rm -rf $TMP_DIR
        #rm ${DIR}/*.bam

        echo
    done
done
