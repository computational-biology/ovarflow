#!/usr/bin/env bash

mkdir Fastq
mkdir Reference

# let's use Gallus gallus as an example

# genome and annotation
wget --background -P Reference https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/315/GCF_000002315.6_GRCg6a/GCF_000002315.6_GRCg6a_genomic.fna.gz
wget --background -P Reference https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/315/GCF_000002315.6_GRCg6a/GCF_000002315.6_GRCg6a_genomic.gff.gz

# Sample data: study PRJNA291174
# https://www.ebi.ac.uk/ena/data/view/PRJNA291174
# Illumina HiSeq 2000, paired end

# reads 1
# base count: 15,111,371,000 => ~14-15 x coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR213/008/SRR2131198/SRR2131198_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR213/008/SRR2131198/SRR2131198_2.fastq.gz
# reads 2
# base count: 12,931,660,000 => ~12 x coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR213/009/SRR2131199/SRR2131199_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR213/009/SRR2131199/SRR2131199_2.fastq.gz
# reads 3
# base count: 10,663,391,800 => ~10 x coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR213/001/SRR2131201/SRR2131201_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR213/001/SRR2131201/SRR2131201_2.fastq.gz
# reads 4
# base count: 14,309,435,800 => ~13-14 x coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR213/002/SRR2131202/SRR2131202_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR213/002/SRR2131202/SRR2131202_2.fastq.gz
