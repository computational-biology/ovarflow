#!/usr/bin/env bash

mkdir Fastq
mkdir Reference

# let's use Gallus gallus as an example

# genome and annotation
wget --background -P Reference https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/315/GCF_000002315.6_GRCg6a/GCF_000002315.6_GRCg6a_genomic.fna.gz
wget --background -P Reference https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/315/GCF_000002315.6_GRCg6a/GCF_000002315.6_GRCg6a_genomic.gff.gz

# Sample data: study PRJEB12944
# https://www.ebi.ac.uk/ena/data/view/PRJEB12944
# Illumina HiSeq 2500, paired end

# reads 1
# base count: 27,777,226,200
# samtools depth & awk: 25 x coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR130/009/ERR1303579/ERR1303579_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR130/009/ERR1303579/ERR1303579_2.fastq.gz
# reads 2
# base count: 27,062,128,250
# samtools depth & awk: 23 x coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR130/003/ERR1303583/ERR1303583_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR130/003/ERR1303583/ERR1303583_2.fastq.gz
# reads 3
# base count: 29,936,943,900
# samtools depth & awk: 24 x coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR130/004/ERR1303584/ERR1303584_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR130/004/ERR1303584/ERR1303584_2.fastq.gz
# reads 4
# base count: 31,033,964,500
# samtools depth & awk: 26 x coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR130/006/ERR1303586/ERR1303586_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR130/006/ERR1303586/ERR1303586_2.fastq.gz
