#!/usr/bin/env bash

mkdir Fastq
mkdir Reference

# let's use Gallus gallus as an example

# genome and annotation
wget --background -P Reference https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/315/GCF_000002315.6_GRCg6a/GCF_000002315.6_GRCg6a_genomic.fna.gz
wget --background -P Reference https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/315/GCF_000002315.6_GRCg6a/GCF_000002315.6_GRCg6a_genomic.gff.gz

# Sample data: study PRJNA306389
# https://www.ebi.ac.uk/ena/data/view/PRJNA306389
# Illumina HiSeq 2500, paired end,  bp reads


# base count: 32,471,461,250
# samtools depth & awk: 25 x coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR304/005/SRR3041115/SRR3041115_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR304/005/SRR3041115/SRR3041115_2.fastq.gz

# base count: 14,567,264,750
# samtools depth & awk: 11 x coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR304/006/SRR3041116/SRR3041116_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR304/006/SRR3041116/SRR3041116_2.fastq.gz

# base count: 18,799,906,500
# samtools depth & awk: 16 x coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR304/003/SRR3041413/SRR3041413_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR304/003/SRR3041413/SRR3041413_2.fastq.gz

# base count: 38,136,658,250
# samtools depth & awk: 34 coverage
wget --background -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR304/007/SRR3041137/SRR3041137_1.fastq.gz
wget -P Fastq ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR304/007/SRR3041137/SRR3041137_2.fastq.gz
