#!/usr/bin/env bash

cp ../OVarFlow_src/Snakefile ./
cp ../OVarFlow_src/OVarFlow_dependencies_mini.yml ./

# the Docker image should contain some additional applications
echo "  - time=1.8" >> OVarFlow_dependencies_mini.yml

cp -r ../OVarFlow_src/scripts ./

docker build -t ovarflow/testing:`date +%b%d_%G` -f Dockerfile_OVarFlow .
