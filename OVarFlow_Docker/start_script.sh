#!/bin/bash

# get the number of cores (threads) on the machine and subtract 4 cores
# creating a default value if no other is provided
cores=`lscpu | grep "^CPU(s):" |  awk '{print($2)}'`
if (( $cores > 4 ))
then
	cores=$((cores - 4))
fi

# is the THREADS variable set by the user?
if [[ -v THREADS ]]
then
	# is the THREADS variable really a number?
	if [[ "$THREADS" =~ ^[0-9]+$ ]]
	then
		threads=${THREADS}
	else
		echo "You provided a TRHEADS varaible but it's not a number:"
		echo "  ->${THREADS}<-"
		echo -e "Using default settings!\n"
		threads=$cores
	fi
else
	echo -e "No THREADS variable set. Using default settings.\n"
	threads=$cores
fi

echo "Using the following number of threads:"
echo "  ->${threads}<-"
echo -e "Starting OVarFlow now:\n\n"

cd /input
# Debugging: output of working dir:
#pwd
snakemake -p -j ${threads} --snakefile /snakemake/Snakefile
