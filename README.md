Welcome to OVarFlow!
=======================

OVarFlow is an Open source Variant calling workFlow for the discovery of SNVs (single nucleotide variants) and indels (insertions and deletions) utilizing short read next generation sequencing data. It is based upon the Genome Analysis Toolkit 4 (GATK) and the GATK Best Practice Workflow for "Germline short variant discovery (SNPs + Indels)". However the original Best Practice Workflow is intended for the evaluation of human short read sequencing data. OVarFlow has been designed to be broadly applicable for model as well as non-model diploid organisms.

An extensive documentation is available at:

* [Read the Docs](https://ovarflow.readthedocs.io/en/latest/).

Container images bundling all required software components are available at:

* [Docker Hub](https://hub.docker.com/r/ovarflow/release/tags) (Docker images)

* [Zenodo](https://zenodo.org/record/4746639) (Singularity containers)

The final peer-reviewed publication is:

* Bathke, J., Lühken, G. OVarFlow: a resource optimized GATK 4 based Open source Variant calling workFlow. BMC Bioinformatics 22, 402 (2021). [https://doi.org/10.1186/s12859-021-04317-y](https://doi.org/10.1186/s12859-021-04317-y).

A preprint was made available at the bioRxiv:

* [OVarFlow: a resource optimized GATK 4 based Open source Variant calling workFlow](https://doi.org/10.1101/2021.05.12.443585)
