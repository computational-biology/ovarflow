========
Citation
========

If you're using OVarFlow or if the documentation provided here was helpful in your own work, please consider citing OVarFlow. The final peer-reviewed publication is:

Bathke, J., Lühken, G. OVarFlow: a resource optimized GATK 4 based Open source Variant calling workFlow. BMC Bioinformatics 22, 402 (2021). `https://doi.org/10.1186/s12859-021-04317-y <https://doi.org/10.1186/s12859-021-04317-y>`_.

A preprint was made available at the `BioRxiv <https://www.biorxiv.org>`_:

Jochen Bathke, Gesine Lühken. OVarFlow: a resource optimized GATK 4 based Open source Variant calling workFlow. 2021. `https://doi.org/10.1101/2021.05.12.443585 <https://doi.org/10.1101/2021.05.12.443585>`_.

=======
License
=======

* The source code of OVarFlow itself is licensed under the terms of the `GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>`_.
* The accompanying documentation of OVarFlow (the document you're reading right now) is licensed under the terms of the `Creative Commons <https://creativecommons.org/>`_ license `CC-BY SA 3.0 <https://creativecommons.org/licenses/by-sa/3.0/de/>`_, either in your national translation or if not applicable in the `global version <https://creativecommons.org/licenses/by-sa/3.0/>`_.

=======
Contact
=======

OVarFlow has been developed at the `Professorship of Pet and Pathogenetics <https://www.uni-giessen.de/fbz/fb09/institute/ith/ag-luehken>`_ at the Justus-Liebig-University Giessen. In case of comments or questions about OVarFlow, contact the institute. Due to high amounts of spam e-mail addresses cannot be posted publicly.

==========
Repository
==========

The source code of OVarFlow and its accompanying documentation can be found at:

* `GitLab <https://gitlab.com/computational-biology/ovarflow>`_

Prebuild containers with all nessessary software components can be found at:

* `Docker hub <https://hub.docker.com/r/ovarflow/release/tags>`_ (Docker images)
* `Zenodo.org <https://zenodo.org/record/4746639>`_, doi: 10.5281/zenodo.4746639 (Singularity containers)
